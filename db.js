const mongoose = require('mongoose');
const logger = require('./logger');

const connect = async () => {
  try {
    await mongoose.connect(
      `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASS}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      },
    );
  } catch (error) {
    logger.error(error);
    process.exit();
  }
};

module.exports = connect;
