const app = require('./app');

require('./logger')();
require('dotenv').config();
// require('./db')()

require('./routes/pdu-parser')(app);

const PORT = process.env.PORT || 8081;

app.listen(PORT);
