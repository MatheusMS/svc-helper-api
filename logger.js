const { createLogger, format, transports } = require('winston');

const {
  combine, timestamp, json, prettyPrint,
} = format;

module.exports = () => {
  global.logger = createLogger({
    level: 'info',
    format: combine(timestamp(), json()),
    defaultMeta: { service: 'user-service' },
    transports: [
      new transports.File({ filename: 'logs/error.log', level: 'error' }),
      new transports.File({ filename: 'logs/combined.log' }),
    ],
  });

  if (process.env.NODE_ENV !== 'production') {
    global.logger.add(
      new transports.Console({
        format: prettyPrint(),
      }),
    );
  }
};
