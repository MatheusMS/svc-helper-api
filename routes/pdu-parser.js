const util = require("util");
const execFile = util.promisify(require("child_process").execFile);
const { readdir } = require("fs").promises;
const fs = require("fs");

module.exports = (app) => {
  app.get("/parsers", (req, res) => {
    readdir("parsers")
      .then((filenames) => {
        const re = new RegExp(".*Parser$");

        const parsers = filenames
          .filter((filename) => re.test(filename))
          .map((value) => value.substring(0, value.length - 6));

        res.status(200).json(parsers);
      })
      .catch((reason) => {
        res.status(400).json(reason);
      });
  });

  app.post("/parse-packets", (req, res) => {
    const fileName = `parsers/${req.body.protocol}Parser`;

    if (!fs.existsSync(fileName)) {
      return res.status(400).json({ error: "Parser does not exist" });
    }

    const promises = [];

    req.body.packets.forEach((packet) => {
      // Sanitize input by removing preceding date
      packet = packet.replace(
        /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2}).*: /,
        ""
      );

      let args = req.body.args || [];

      args = args.concat(["-p", packet]);

      promises.push(execFile(fileName, args));
    });

    Promise.allSettled(promises).then((responses) => {
      const result = responses.map((response) => {
        if (response.value) {
          return response.value.stdout || response.value.stderr;
        }

        return response.reason.stdout || response.reason.stderr;
      });

      res.status(200).send(result);
    });
  });
};
